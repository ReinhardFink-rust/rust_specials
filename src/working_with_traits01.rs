use std::cmp::Ordering;

#[derive(Debug, Copy, Clone)]
struct Kiste {
    l : i32,
    b : i32,
    h : i32,
}

impl Kiste {

    fn new(l : i32, b : i32, h : i32) -> Kiste {
        Kiste {l,b,h}
    }
}

impl PartialEq for Kiste {

    fn eq(&self, other: &Self) -> bool{
        self.l == other.l && self.b == other.b && self.h == other.h
    }

    fn ne(&self, other: &Self) -> bool {
        !self.eq(other)
    }
}

impl PartialOrd for Kiste {

    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.eq(other) {
            Some(Ordering::Equal)
        }
        else if self.l * self.b * self.h > other.l * other.b * other.h {
            Some(Ordering::Greater)
        }
        else if self.l * self.b * self.h < other.l * other.b * other.h {
            Some(Ordering::Less)
        }
        else {
            None
        }
    }

    fn lt(&self, other: &Self) -> bool {
        match self.partial_cmp(other) {
            Some(Ordering::Less) => return true,
            _                     => return false,
        }
    }

    fn le(&self, other: &Self) -> bool {
        self.eq(other) || self.lt(other)
    }

    fn gt(&self, other: &Self) -> bool {
        !self.le(other)
    }

    fn ge(&self, other: &Self) -> bool {
        !self.lt(other)
    }
}


fn bubble_sort_i32(list : &mut [i32]) {
    println!("Bubblesort für i32");
    for _runs in 0..list.len()-1 {
        for i in 0..list.len()-1 {
            if list[i+1] < list[i] {
                let temp = list[i];
                list[i] = list[i+1];
                list[i+1] = temp;
            }
        }
    }
}

fn bubble_sort_t_copy<T : std::cmp::PartialOrd + std::marker::Copy>(list : &mut [T]) {
    println!("Bubblesort für T with Copy");
    for _runs in 0..list.len()-1 {
        for i in 0..list.len()-1 {
            if list[i+1] < list[i] {
                // swap with copy of elements
                let temp = list[i];
                list[i] = list[i+1];
                list[i+1] = temp;
            }
        }
    }
}

fn bubble_sort_t<T : std::cmp::PartialOrd>(list : &mut [T]) {
    println!("Bubblesort für T without Copy");
    for _runs in 0..list.len()-1 {
        for i in 0..list.len()-1 {
            if list[i+1] < list[i] {
                // swap with internal swap of pointers
                // because:
                // let temp = list[i];
                //            ^^^^^^^ cannot move out of here
                //                    move occurs because `list[_]` has type `T`, which does not implement the `Copy` trait
                // list[i] = list[i+1];
                // list[i+1] = temp;
                list.swap(i,i+1);
            }
        }
    }
}

pub fn working_with_traits01() {
    println!("working with traits 01");
    // i32
    let mut list : [i32;10] = [7,3,5,8,9,1,2,4,6,0];
    println!("Ursprüngliche Liste: {:?}", list);
    bubble_sort_i32(&mut list);
    println!("Sortierte Liste:     {:?}", list);

    // i32 generic
    let mut list : [i32;10] = [7,3,5,8,9,1,2,4,6,0];
    println!("Ursprüngliche Liste T1: {:?}", list);
    bubble_sort_t_copy(&mut list);
    println!("Sortierte Liste T1:     {:?}", list);

    // Kiste with copy
    let k1 = Kiste::new(1,1,1);
    let k2 = Kiste::new(1,1,1);
    let k3 = Kiste::new(1,1,2);
    assert_eq!(k1.eq(&k2),true);
    assert_eq!(k1.eq(&k3),false);
    assert_eq!(k1.ne(&k3),true);
    assert_eq!(k1.lt(&k2),false);
    assert_eq!(k1.lt(&k3),true);
    assert_eq!(k1.le(&k2),true);

    let mut list = [Kiste::new(1,1,7),
        Kiste::new(1,1,3),
        Kiste::new(1,1,5),
        Kiste::new(1,1,7),
        Kiste::new(1,1,8),
        Kiste::new(1,1,9),
        Kiste::new(1,1,1),
        Kiste::new(1,1,2),
        Kiste::new(1,1,4),
        Kiste::new(1,1,6),
        Kiste::new(1,1,0)];
    println!("Ursprüngliche Liste T1: {:#?}", list);
    bubble_sort_t_copy(&mut list);
    println!("Sortierte Liste T1:     {:#?}", list);

    let mut list = [Kiste::new(1,1,7),
        Kiste::new(1,1,3),
        Kiste::new(1,1,5),
        Kiste::new(1,1,7),
        Kiste::new(1,1,8),
        Kiste::new(1,1,9),
        Kiste::new(1,1,1),
        Kiste::new(1,1,2),
        Kiste::new(1,1,4),
        Kiste::new(1,1,6),
        Kiste::new(1,1,0)];
    println!("Ursprüngliche Liste T1: {:#?}", list);
    bubble_sort_t(&mut list);
    println!("Sortierte Liste T1:     {:#?}", list);
}