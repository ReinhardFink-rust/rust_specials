
pub fn working_with_functions() {

    println!("working with functions");

    let _immut = 1;
    get_immutable_var(_immut);
    get_mutable_var(_immut);
    // not working
    // get_mutable_ref(_immut);
    // get_mutable_ref(mut _immut); //even wrong syntax
    // get_mutable_ref(&mut _immut);
    println!("out: {}", _immut);

    let mut _mut = 10;
    get_mutable_ref(&mut _mut);
    println!("out: {}", _mut);

    let mut _mut2 = 12;
    get_mutable_ref_twice(&mut _mut, &mut _mut2);
}

fn get_immutable_var(i : i32) {
    // i immutable copy
    //i = i + 1; // not working :-(
    println!("{}", i);
}

fn get_mutable_var(mut i : i32) {
    // i mutable copy
    i += 1;
    println!("{}", i);
}

fn get_mutable_ref(i : &mut i32) {
    // i mutable reference
    // i += 1; // not working
    *i += 1; // dereference needed
    println!("{}", i);
    println!("{}", *i); // i the same as *i
}

fn get_mutable_ref_twice(i : &mut i32, j : &mut i32) {
    println!("normal: {}, {}", i, j);
    let temp = *i;
    *i = *j;
    *j = temp;
    println!("exchanged: {}, {}", i, j);
}