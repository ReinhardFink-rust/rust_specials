mod drawable;
mod draw_objects;

use std::rc::Rc;
use std::cell::RefCell;
use crate::working_with_traits02::drawable::Drawable;

use crate::working_with_traits02::draw_objects::{Button, Checkbox, TextField};

pub fn working_with_traits02() {
    components_as_single_elements();

    components_in_a_box_to_create_a_list();

    components_in_a_rc_to_get_immutable_access_twice();

    components_in_a_rc_refcell_to_get_mutable_access_twice();
}


fn components_as_single_elements() {
    println!();
    println!("Components as single elements");
    let label_start = "Start";
    let b01 = Button::new(0, 0, 10, 20, label_start);
    b01.draw();
    b01.draw(); // but with draw(&self)
    let b02 = Button::new(100, 100, 10, 20, "Stop");
    b02.draw();
    let c01 = Checkbox::new(200, 200, 10, 20, "Essen", false);
    c01.draw();
    let c02 = Checkbox::new(300, 300, 10, 20, "Trinken", true);
    c02.draw();
}

fn components_in_a_box_to_create_a_list() {
    println!();
    println!("Componentes as Drawable List with Box");
    /*
    A pointer type for heap allocation.
    Box<T>, casually referred to as a ‘box’, provides the simplest form of heap allocation in Rust.
    Boxes provide ownership for this allocation, and drop their contents when they go out of scope.
    Boxes also ensure that they never allocate more than isize::MAX bytes.
     */
    // we need a Box, to put all components in a list
    let b01 = Button::new(0, 0, 10, 20, "Move");
    let b02 = Button::new(100, 100, 10, 20, "Turn");
    let c01 = Checkbox::new(200, 200, 10, 20, "Jump", false);
    let c02 = Checkbox::new(300, 300, 10, 20, "Sink", true);
    let mut component_list: Vec<Box<dyn Drawable>> = vec!();
    component_list.push(Box::new(b01));
    component_list.push(Box::new(b02));
    component_list.push(Box::new(c01));
    component_list.push(Box::new(c02));
    let mut t01 = TextField::new(400, 400, 50, 50, String::from("Das wird der Inhalt"));
    t01.add('#');
    component_list.push(Box::new(t01));

    for d in component_list.iter() {
        d.draw()
    }
    for d in &component_list {
        d.draw()
    }
    //t01.draw(); //moved :-(
}

fn components_in_a_rc_to_get_immutable_access_twice() {
    println!();
    println!("Componentes as Drawable List with Rec");
    /*
    Single-threaded reference-counting pointers. ‘Rc’ stands for ‘Reference Counted’.
    The type Rc<T> provides shared ownership of a value of type T, allocated in the heap.
    Invoking clone on Rc produces a new pointer to the same allocation in the heap.
    When the last Rc pointer to a given allocation is destroyed,
    the value stored in that allocation (often referred to as “inner value”) is also dropped.

    Shared references in Rust disallow mutation by default, and Rc is no exception:
    you cannot generally obtain a mutable reference to something inside an Rc.
    If you need mutability, put a Cell or RefCell inside the Rc;
    see an example of mutability inside an Rc.
     */
    let b01 = Button::new(0, 0, 10, 20, "Move");
    let b02 = Button::new(100, 100, 10, 20, "Turn");
    let c01 = Checkbox::new(200, 200, 10, 20, "Jump", false);
    let c02 = Checkbox::new(300, 300, 10, 20, "Sink", true);
    let mut component_list: Vec<Rc<dyn Drawable>> = vec!();
    component_list.push(Rc::new(b01));
    component_list.push(Rc::new(b02));
    component_list.push(Rc::new(c01));
    component_list.push(Rc::new(c02));
    let mut t01 = TextField::new(400, 400, 50, 50, String::from("Das wird der Inhalt"));
    t01.add('#');
    // we need downcast to Drawable :-(
    let rc_t11: Rc<dyn Drawable> = Rc::new(t01);
    component_list.push(Rc::clone(&rc_t11));
    for d in component_list.iter() {
        d.draw()
    }
    rc_t11.draw(); // with a saved reference
}

fn components_in_a_rc_refcell_to_get_mutable_access_twice() {
    println!();
    println!("Componentes as Drawable List with Rc<RefCell<>>");
    /*
    Shareable mutable containers.
    Rust memory safety is based on this rule: Given an object T,
    it is only possible to have one of the following:

    Having several immutable references (&T) to the object (also known as aliasing).
    Having one mutable reference (&mut T) to the object (also known as mutability).

    This is enforced by the Rust compiler.
    However, there are situations where this rule is not flexible enough.
    Sometimes it is required to have multiple references to an object and yet mutate it.

    Shareable mutable containers exist to permit mutability in a controlled manner,
    even in the presence of aliasing. Both Cell<T> and RefCell<T>
    allow doing this in a single-threaded way.
    However, neither Cell<T> nor RefCell<T> are thread safe (they do not implement Sync).
    If you need to do aliasing and mutation between multiple threads
    it is possible to use Mutex<T>, RwLock<T> or atomic types.
     */
    let b01 = Button::new(0, 0, 10, 20, "Move");
    let b02 = Button::new(100, 100, 10, 20, "Turn");
    let c01 = Checkbox::new(200, 200, 10, 20, "Jump", false);
    let c02 = Checkbox::new(300, 300, 10, 20, "Dive", true);
    let mut component_list: Vec<Rc<RefCell<dyn Drawable>>> = vec!();
    component_list.push(Rc::new(RefCell::new(b01)));
    component_list.push(Rc::new(RefCell::new(b02)));
    component_list.push(Rc::new(RefCell::new(c01)));
    component_list.push(Rc::new(RefCell::new(c02)));
    let mut t01 = TextField::new(400, 400, 50, 50, String::from("Das wird der Inhalt"));
    t01.add('#');
    // we create a Rc<RefCell<TextField>> for use after draw all Drawables
    let rc_refcell_t01 = Rc::new(RefCell::new(t01));
    // we need downcast to Drawable :-(
    let rc_refcell_t02: Rc<RefCell<dyn Drawable>> = rc_refcell_t01.clone();
    component_list.push(Rc::clone(&rc_refcell_t02));
    for d in component_list.iter() {
        d.borrow().draw();
    }
    rc_refcell_t01.borrow().draw(); // with a saved reference immutable
    rc_refcell_t01.borrow_mut().add(' '); // with a saved reference mutable
    rc_refcell_t01.borrow_mut().add(':'); // with a saved reference mutable
    rc_refcell_t01.borrow_mut().add('-'); // with a saved reference mutable
    rc_refcell_t01.borrow_mut().add(')'); // with a saved reference mutable
    rc_refcell_t01.borrow().draw();
    // we can't get back a textField from Drawable ???
    match component_list.get(3) {
        Some(component) => component.borrow().draw(),
        None => (),
    };
}

