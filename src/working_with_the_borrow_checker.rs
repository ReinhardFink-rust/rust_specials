
#[derive(Debug)]
struct Container {
    us : usize,
    list : Vec<usize>
}

#[allow(dead_code)]
impl Container {
    fn new() -> Self {
        Container {
            us: 0,
            list: vec![0,1,2,3],
        }
    }

    fn read_us(&self) -> usize {
        self.us
    }

    fn inc_us(&mut self) {
        self.us += 1;
    }

    fn inc_list(&mut self, i : usize) {
        self.list[i] += 1;
    }

    fn inc_list_all1(&mut self) {
        for l_i in self.list.iter_mut() {
            // not allowed by borrow checker:
            // self.list[0] += 1;
            // but we can access over iter_mut
            *l_i += 1;
            // not allowed by borrow checker:
            // self.inc_us();
            // because inc_us() uses self mutable,
            // but we can access direct:
            self.us += 1;
            // not allowed by borrow checker:
            // self.read_us();
            // but we can access direct:
            let _a = self.us;
        }
    }

    fn inc_list_all2(&mut self) {
        for i in 0..self.list.len() {
            self.list[i] += 1;
            self.inc_us();
            let _a = self.read_us();
        }
    }

    fn inc_list_all3(&mut self) {
        self.list.iter_mut().for_each(|elm| *elm += 1 );
    }
}
pub(crate) fn working_with_the_borrow_checker() {
    let mut _c = Container::new();
    println!("{:?}",_c);
    _c.inc_list_all1();
    println!("inc_all_1 {:?}",_c);
    _c.inc_list_all2();
    println!("inc_all_2 {:?}",_c);
    _c.inc_list_all3();
    println!("inc_all_3 {:?}",_c);

}