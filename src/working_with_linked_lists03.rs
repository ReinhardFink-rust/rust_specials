/*
    We want to transform following C - program
    into a RUST - program

    have a look at this very good manual:
    https://rust-unofficial.github.io/too-many-lists/
 */

/*
// A simple C program for traversal of a linked list
#include <stdio.h>
#include <stdlib.h>

struct _Node {
    int data;
    struct _Node* next;
};

typedef struct _Node Node;

// This function prints contents of linked list starting from
// the given node
void printList(Node* current)
{
    printf("Printing List:\n");
    while (current != NULL) {
        printf("%d \n", current->data);
        current = current->next;
    }
}

int main()
{

    // variables
    Node* head;
    Node* new_node;
    Node* current;

    // create list
    head = NULL;

    // ADD PART:

    // add first element to head
    head = (Node*) malloc(sizeof(Node));
    head->data = 1;
    head->next = NULL;

    // add second element to head of list
    new_node = (Node*) malloc(sizeof(Node));
    new_node->data = 0;
    new_node->next = head;
    head = new_node;

    // add third element to tail of the list
    current = head;
    while (current->next != NULL) {
        current = current->next;
    }
    new_node = (Node*) malloc(sizeof(Node));
    new_node->data = 2;
    new_node->next = NULL;
    current->next = new_node;

    // add fourth element to tail of the list
    current = head;
    while (current->next != NULL) {
        current = current->next;
    }
    new_node = (Node*) malloc(sizeof(Node));
    new_node->data = 3;
    new_node->next = NULL;
    current->next = new_node;

    printList(head);

    // DROP PART:

    // drop last element from list
    current = head;
    while (current->next->next != NULL) {
        current = current->next;
    }
    free(current->next);
    current->next = NULL;

    printList(head);

    //drop first element from list
    current = head;
    head = head->next;
    free(current);

    printList(head);

    //drop first element from list
    current = head;
    head = head->next;
    free(current);

    printList(head);

    //drop first element from list
    current = head;
    head = head->next;
    free(current);

    printList(head);

    //drop first element from empty list, will break at runtime

return 0;
}

*/


/*
    ################################################################################
    ######## START
    ################################################################################
 */

/*
    Problem #1:
    we don't have NULL
    so
    while (current->next != NULL)
    is not possible :-(

    see: https://doc.rust-lang.org/book/ch06-01-defining-an-enum.html?highlight=option#the-option-enum-and-its-advantages-over-null-values
 */

/*
    Problem #2:
    How does RUST allocate memory on the heap?

    see: https://doc.rust-lang.org/book/ch15-01-box.html?highlight=box#enabling-recursive-types-with-boxes
 */

/* C - code
    struct _Node {
        int data;
        struct _Node* next;
    };
 */

/*
    we try with a direct translation of _Node.
    Option with Some<T> ond None covers NULL
    ATTENTION:
    next: Box<Option<Node>> will push NULL = None into the Box
 */

#[derive(Debug)]
pub struct Node {
    data: i32,
    next: Option<Box<Node>>,
}
// we can imagine Link as Option<Box<Node>>
type _Link = Option<Box<Node>>;

/* C - code
void printList(Node* current)
{
    printf("Printing List:\n");
    while (current != NULL) {
        printf("%d \n", current->data);
        current = current->next;
    }
}
 */

// we are not able to get Node out of Option without match :-(
//
pub fn _print_list_broken(current : &Option<Box<Node>>) {
    if current.is_none() {
        println!("Empty List!");
    }
    while current.is_some() {
        println!("{:?}", current.iter().next());
        // not able to get node in Option :-(
        // therefore we can not "increment current-pointer"
    }
}

// &Option<Box<Node>>: & otherwise Option<Box<Node>> moves into fn
// "current" works as mutable reference
pub fn print_list_long(mut current : &Option<Box<Node>>)
{
    println!("Printing List (long version):");
    if current.is_none() {
        println!("Empty List!");
    }
    loop {
        match current {
            None => {
                return
            },
            // Some is Some<&Box<Node>> from &Option<Box<Node>>
            Some(node) => {
                println!("{}", node.data);
                current = &node.next;
            }
        };
    }
}

pub fn _print_list_short(mut current : &Option<Box<Node>>)
{
    println!("Printing List:");
    if current.is_none() {
        println!("Empty List!");
    }
    while let Some(node) = current {
        println!("{}", node.data);
        current = &node.next;
    }
}

pub fn _print_list_with_suboptimal_parameter(head : &Option<Box<Node>>)
{
    let mut current : &Option<Box<Node>> = head;
    println!("Printing List:");
    if current.is_none() {
        println!("Empty List!");
    }
    while let Some(node) = current {
        println!("{}", node.data);
        current = &node.next;
    }
}

// with "optimal" parameter head
pub fn print_list(head : &Box<Option<Box<Node>>>)
{
    // ATTENTION
    // let mut current = head;
    // leads to
    // let mut current :  &Box<Option<Box<Node>>> = head;
    // and will break   while let Some(node) ...
    // so we get 2 working version
    // version 1:
    // let mut current :  &Option<Box<Node>> = head;
    // or with dereference twice and creating a reference
    // version 2:
    // let mut current = &**head;
    //                     ^ &Box<Option<Box<Node>>> -> Box<Option<Box<Node>>>
    //                    ^  Box<Option<Box<Node>>> -> Option<Box<Node>>
    //                   ^   Option<Box<Node>> -> &Option<Box<Node>>
    // version 3:
    let mut current = head.as_ref();
    println!("Printing List:");
    if current.is_none() {
        println!("Empty List!");
    }
    while let Some(node) = current {
        println!("{}", node.data);
        current = &node.next;
    }
}

pub fn prepend_to_list_with_return_value(head :  &mut Option<Box<Node>>, data : i32) -> Box<Option<Box<Node>>>{
    //let mut current = node;
    match head {
        _ => {
            Box::new(Option::Some(Box::new(Node { data, next : head.take() })))
        },
    }
}


// head: eine Variable, die einen veränderbaren Zeiger enthält, head selbst ist aber unveränderbar
pub fn prepend_to_list__(head:  &mut Box<Option<Box<Node>>>, data : i32) {
    **head = Option::Some(Box::new(Node { data, next : head.take() }));
}

pub fn prepend_to_list_(head:  &mut Box<Option<Box<Node>>>, data : i32) {
    // **match head  // works too,
    // so we have automatic dereference??
    match head {
        _ => {
            // this construction cost me about 10 hours :-(
            // **head
            //  ^ &mut Box<Option<Box<Node>>> -> Box<Option<Box<Node>>>
            // ^  Box<Option<Box<Node>>> - > Option<Box<Node>>
            **head = Option::Some(Box::new(Node { data, next : head.take() }));
        },
    }
}

pub fn prepend_to_list(head:  &mut Box<Option<Box<Node>>>, data : i32) {
    //head.as_deref().replace(Option::Some(Box::new(Node { data, next : head.take() })));
    match head {
        _ => {
            **head = Option::Some(Box::new(Node { data, next : head.take() }));
            //**head.replace( Option::Some(Box::new(Node { data, next : head.take() })));
        },
    }
}


pub fn working_with_linked_lists03() {

    println!();
    println!("working with lists 03");
    // create empty list
    let mut head : Box<Option<Box<Node>>> = Box::new(Option::None);
    print_list(&head);
    print_list_long(&*head);

    // prepend node to list
    // ATTENTION:
    // *head (= Option<Node>) is moved and the list starts from current
    // current (= Box) is then moved to head (=Box)
    let current : Box<Option<Box<Node>>> = Box::new(Option::Some(Box::new(Node {data : 0, next : *head})));
    head = current;
    print_list(&head);
    print_list_long(&*head);

    // prepend node to list
    let current : Box<Option<Box<Node>>> = Box::new(Option::Some(Box::new(Node {data : 1, next : *head})));
    head = current;
    print_list(&head);
    print_list_long(&*head);

    // prepend node to list
    let current : Box<Option<Box<Node>>> = Box::new(Option::Some(Box::new(Node {data : 2, next : *head})));
    head = current;
    print_list(&head);
    // print_list(&current);
    print_list_long(&*head);

    head = prepend_to_list_with_return_value(&mut head, 3);
    print_list(&head);

    prepend_to_list(&mut head, 4);
    print_list(&head);

    prepend_to_list(&mut head, 5);
    print_list(&head);

    prepend_to_list_(&mut head, 6);
    print_list(&head);

    prepend_to_list__(&mut head, 7);
    print_list(&head);

}




