
#[macro_export]
macro_rules! vec1 {
    ( $( $x:expr ),* ) => {
        {
            let mut temp_vec = Vec::new();
            $(
                temp_vec.push($x);
            )*
            temp_vec
        }
    };
}

#[macro_export]
macro_rules! vec2 {
    ( $( $x:expr ),* ) => {
        {
            let mut temp_vec = Vec::new();
            $(
                temp_vec.push($x);
                println!("push: {}",$x);
            )*
            temp_vec
        }
    };
}
#[macro_export]
macro_rules! clone1 {
    (@param _) => ( _ );
}

#[macro_export]
macro_rules! clone2 {
    (@param _) => ( _ );
    (@param $x:ident) => ( $x );
}

#[macro_export]
macro_rules! clone3 {
    (@param _) => ( _ );
    (@param $x:ident) => ( $x );
    ($($n:ident),+ => move || $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move || $body
        }
    );
}

#[macro_export]
macro_rules! clone4 {
    (@param _) => ( _ );
    (@param $x:ident) => ( $x );
    ($($n:ident),+ => move || $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move || $body
        }
    );
    ($($n:ident),+ => move |$($p:tt),+| $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move |$(clone4!(@param $p),)+| $body
        }
    );
}


pub fn working_with_macros() {
    println!("MACROS!");

    // Used as an expression.
    let _x = vec![1, 2, 3];

    // Used as a statement.
    println!("Hello!");

    // Used in a pattern.
    macro_rules! pat {
        ($i:ident) => (Some($i))
    }

    if let pat!(_x) = Some(1) {
        assert_eq!(_x, 1);
    }

    // Used in a type.
    macro_rules! Tuple {
        { $A:ty, $B:ty } => { ($A, $B) };
    }
    #[allow(dead_code)]
    type N2 = Tuple!(i32, i32);

    // Used as an item.
    //thread_local!(static FOO: RefCell<u32> = RefCell::new(1));

    // Used as an associated item.
    macro_rules! const_maker {
        ($t:ty, $v:tt) => { const CONST: $t = $v; };
    }
    #[allow(dead_code)]
    trait T {
        const_maker! {i32, 7}
    }

    // Macro calls within macros.
    macro_rules! example {
            () => { println!("Macro call in a macro!") };
    }
    // Outer macro `example` is expanded, then inner macro `println` is expanded.
    example!();

    #[allow(unused_macros)]
    macro_rules! what {
        ($($n:ident),+ => move || $body:expr) => (
            {
                $( let $n = $n.clone(); )+
                move || $body
            }
        );
    }

    //what!((1,2));

    let _v0: Vec<i64> = vec1!(0,1,2);
    //let a = clone1!(@param _); expanded to_ let a = _;

    let v1: Vec<i64> = vec!(1, 2, 3);
    let _a = clone2!(@param v1); // expanded to: let a = v1;

    let v2: Vec<i64> = vec!(2, 3, 4);
    let _a = clone3!(@param v2); // expanded to: let a = v1;

    let v3: Vec<i64> = vec!(3, 4, 5);
    let _a = clone3!(v3 => move || { println!("{:?}",v3); }); //expanded to let v3 = v3.clone();  move || { ... }

    let _v4: Vec<i64> = vec!(4, 5, 6);
    // does not compile
    //let a = clone4!(v4 => move |x| { println!("{:?},{:?}",v4,x); }); //expanded to let v4 = v4.clone();  move |x| { ... }
}