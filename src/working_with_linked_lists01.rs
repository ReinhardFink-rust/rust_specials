mod llist;

use crate::working_with_linked_lists01::llist::LListT;

pub fn working_with_linked_lists01() {

    let mut llistt : Box<LListT<&str>> = LListT::new();
    llistt.to_string();
    llistt.append("Emil");
    llistt.to_string();
    llistt.append("Gustl");
    llistt.to_string();
    llistt.append("Karli");
    llistt.to_string();
    llistt.prepend("Pauli");
    llistt.to_string();
    llistt.prepend("Sascha");
    llistt.to_string();
    llistt.prepend_easy("Olli");
    llistt.to_string();
}
