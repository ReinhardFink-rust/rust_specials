mod llist;

use crate::working_with_linked_lists02::llist::{LList};

pub fn working_with_linked_lists02() {

    let mut llist : LList<&str> =  LList::new();
    llist.to_string();
    llist.prepend("World");
    llist.to_string();
    llist.prepend("hello");
    llist.to_string();
/*
    let mut llistt : Box<Node<&str>> = Node::new();
    llistt.to_string();
    llistt.append("Emil");
    llistt.to_string();
    llistt.append("Gustl");
    llistt.to_string();
    llistt.append("Karli");
    llistt.to_string();
    llistt.prepend_long("Pauli");
    llistt.to_string();
    llistt.prepend_long("Sascha");
    llistt.to_string();
    llistt.prepend("Olli");
    llistt.to_string();
*/
}

#[cfg(test)]
mod test {
    use crate::working_with_linked_lists02::llist::{Node, LList};

    #[test]
    fn test_is_empty() {
        let mut llist : LList<&str> =  LList::new();
        assert_eq!(llist.is_empty(),true);
        llist.prepend("hello");
        assert_eq!(llist.is_empty(),false);
        llist.prepend("World");
        assert_eq!(llist.is_empty(),false);
}
}