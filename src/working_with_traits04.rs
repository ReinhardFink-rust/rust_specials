use std::fmt::Display;

pub trait WorkOn {
    type MyInput;
    type MyOutput: Display;

    fn work(&self, input : Self::MyInput) -> Self::MyOutput;
}

struct MyWorkOn {
}

impl WorkOn for MyWorkOn {
    type MyInput = f64;
    type MyOutput = i64;

    fn work(&self, input: Self::MyInput) -> Self::MyOutput {
        input as i64
    }
}

pub fn working_with_traits04() {
    let m_wo = MyWorkOn { };
    println!("{}", m_wo.work(56.12345));
}
