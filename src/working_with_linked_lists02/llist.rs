use std::mem;
use std::fmt::Display;

/*
    we need a enum as a pointer to:
   data : ELEMENT
   nil:   NIL
 */
pub enum Node<T> {
    ELEMENT {
        elem: T,
        next: Box<Node<T>>,
    },
    NIL,
}

/*
    we need a struct for all list functionalities
 */
pub struct LList<T> {
    head : Box<Node<T>>,
}

impl<T> LList<T>
    where T: Display {

    pub fn new() -> LList<T> {
        LList{
            head : Box::new(Node::NIL)
        }
    }

    #[allow(dead_code)]
    pub fn is_empty(&self) -> bool {
        if let Node::NIL = *self.head {
            true
        } else {
            false
        }
    }

    pub fn prepend(&mut self, value: T) {
        println!("Prepending '{}' to LList", value);
        let ptr_old_head: Box<Node<T>> = mem::replace( &mut self.head, Box::new(Node::NIL));
        self.head = Box::new(Node::ELEMENT {
            elem : value,
            next : ptr_old_head,
        })
    }

    pub fn _append(&mut self, _value : T) {
        println!("Appending");
        //let &mut current_node : Node<T> = self.head;
        //evt mit match versuchen
        //let &mut current_node = self.head;
        /*
        while let Node::ELEMENT { elem: _, next } = current_node {
            current_node = next;
        }
        *current_node = Node::ELEMENT { elem: value, next: Box::new(Node::NIL) };
*/

    }
/*
    pub fn get(&self, i : u32) -> Option<&T> {
        let mut index = 0;
        let mut current = self;
        let mut found = Option::None;
        while let Node::ELEMENT { elem, next } = current {
            if index < i {
                index += 1;
                current = next;
            } else if index == i {
                found = Option::Some(elem);
            } else {
                found = Option::None;
            }
        }
        found
    }
*/
    pub fn to_string(&self) {
        println!("Printing LList");
        let mut index = 0;
        /*
            self.head : Box<Node<T>>
            *self.head : Node<T>        <- deref
            &*self.head : &Node<T>      <- ref
         */
        let mut current = &*self.head;
        if let Node::NIL = current {
            println!("empty list");
        } else {
            while let Node::ELEMENT { elem, next } = current {
                println!("{} : {}", index, elem);
                index += 1;
                current = next;
            };
        }
    }
}

