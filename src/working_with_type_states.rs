use std::{collections::HashSet};

const CLEAR: &str = "\x1B[2J\x1B[1;1H";

struct Unbounded;

struct Bounded {
    bound: usize,
    delimiters: (char, char),
}

//TODO: which type is Self
trait ProgressDisplay: Sized {
    fn display<Iter>(&self, progress: &Progress<Iter, Self>);
}

impl ProgressDisplay for Unbounded {
    // works too: fn display<Iter>(&self, progress: &Progress<Iter, Unbounded>) {
    fn display<Iter>(&self, progress: &Progress<Iter, Self>) {
        println!("{}", "*".repeat(progress.i))
    }
}

impl ProgressDisplay for Bounded {
    // works too: fn display<Iter>(&self, progress: &Progress<Iter, Bounded>) {
    fn display<Iter>(&self, progress: &Progress<Iter, Self>) {
        println!("{}{}{}{} i:{} bound:{}",
                 self.delimiters.0,
                 "*".repeat(progress.i),
                 " ".repeat(self.bound - progress.i),
                 self.delimiters.1,
                 progress.i,
                 self.bound)
    }
}

struct Progress<Iter, Bound> {
    iter: Iter,
    i: usize,
    bound: Bound,
}

impl<Iter> Progress<Iter, Unbounded> {
    // progress bar, for all iterators
    fn new(iter: Iter) -> Self {
        Progress {
            iter,
            i: 0,
            bound: Unbounded,
        }
    }
}

impl<Iter> Progress<Iter, Unbounded>
    where Iter: ExactSizeIterator {
    // ATTENTION: Progress<Iter, Bounded> returned from Progress<Iter, Unbounded>
    // State of Progress has changed from Unbounded to Bounded
    // kind of new for bounded iterator
    fn with_bound(self) -> Progress<Iter, Bounded> {
        let bound = Bounded {
            bound: self.iter.len(),
            delimiters: ('[', ']'),
        };
        Progress {
            iter: self.iter,
            i: self.i,
            bound,
        }
    }
}

impl<Iter> Progress<Iter, Bounded> {
    // ATTENTION: with_delimiters(...) can only be called, if bound is from type Bounded
    fn with_delimiters(mut self, delimiters: (char, char)) -> Self {
        self.bound.delimiters = delimiters;
        self
    }
}

impl<Iter, Bound> Iterator for Progress<Iter, Bound>
    where Iter: Iterator, Bound: ProgressDisplay {
    type Item = Iter::Item;

    fn next(&mut self) -> Option<Self::Item> {
        print!("{}", CLEAR);
        self.bound.display(&self);
        self.i += 1;
        self.iter.next()
    }
    /*
    fn next(&mut self) -> Option<Self::Item> {
        print!("{}",CLEAR);
        match self.bound {
            Some(bound) =>
                println!("{}{}{}{} i:{} bound:{}",
                         self.delimiters.0,
                         "*".repeat(self.i),
                         " ".repeat(bound - self.i),
                         self.delimiters.1,
                         self.i,
                         bound),
            None => println!("{}", "*".repeat(self.i))
        }
        self.i += 1;
        self.iter.next()
    }
    */
}

trait ProgressIteratorExt: Sized {
    //fn progress(self) -> Progress<ProgressIteratorExt>;
    //                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ doesn't have a size known at compile-time
    // TODO: which type is Self ??
    fn progress(self) -> Progress<Self, Unbounded>;
}

//works to: impl<T> ProgressIteratorExt for T {
impl<Iter> ProgressIteratorExt for Iter {
    fn progress(self) -> Progress<Self, Unbounded> {
        Progress::new(self)
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////
fn progress<Iter>(iter: Iter, f: fn(Iter::Item) -> ())
    where Iter: Iterator {
    let mut i = 1;
    for n in iter {
        println!("{}{}", CLEAR, "*".repeat(i));
        i += 1;
        f(n);
    }
}


fn expensive_calculation(_n: &i32) {
    //sleep(Duration::from_secs(1));
}

/////////////////////////////////////////////////////////////////////////////////////////////////
pub fn working_with_type_states() {
    println!("working with TYPE STATES, from https://www.youtube.com/watch?v=bnnacleqg6k&t=446s");

    let v = vec![1, 2, 3];

    println!("version 1");
    progress(v.iter(), expensive_calculation);
    let mut h = HashSet::new();
    h.insert(0);
    progress(h.iter(), expensive_calculation);

    println!("version 2");
    for n in Progress::new(v.iter()) {
        expensive_calculation(n);
    }

    println!("version 3");
    //1.progress();
    for n in v.iter().progress() {
        expensive_calculation(n);
    }

    println!("version 4");
    //1.progress();
    for n in v.iter().progress().with_bound() {
        expensive_calculation(n);
    }

    println!("version 5");
    //1.progress();
    for n in v.iter().progress().with_bound().with_delimiters(('<', '>')) {
        expensive_calculation(n);
    }

    println!("version endless");
    // ERROR: for _n in (0..).progress().with_delimiters() {
    // ERROR: for _n in (0..).progress().with_bound() {
    for _n in (0..).progress() {
        expensive_calculation(&_n);
        if _n == 5 {
            break
        }
    }
}