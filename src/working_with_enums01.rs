
enum MyResult<T> {
    OK(T),
    BROKEN,
}

pub fn working_with_enums01() {
    println!("work with enums");

    let _b = MyResult::OK(3);
    match _b {
        MyResult::OK(x) => println!("b = {}", x),
        MyResult::BROKEN => println!("oh je, everything broken ;-("),
    }
    let _c : MyResult<i32> = MyResult::BROKEN;
    match _c {
        MyResult::OK(x) => println!("b = {}", x),
        MyResult::BROKEN => println!("oh je, everything broken ;-("),
    }
    // _d needs: _d : MyResult<i32> to know type for OK(x)
    // let _d = MyResult::BROKEN;
    //          ^^^^^^^^^^^^^^^^ cannot infer type for type parameter `T` declared on the enum `MyResult`
}

