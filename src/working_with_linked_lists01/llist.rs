use std::mem;
use std::fmt::Display;

pub enum LListT<T> {
    Element {
        elem: T,
        next: Box<LListT<T>>,
    },
    Nil,
}

impl<T> LListT<T>
    where T: Display {
    pub fn new() -> Box<LListT<T>> {
        Box::new(LListT::Nil)
    }

    pub fn prepend(&mut self, value: T) {
        println!("Prepending");
        match self {
            LListT::Element { elem: _elem, next: _ } => {
                *self = *Box::new(LListT::Element {
                    elem: value,
                    next: Box::new(mem::replace(self, LListT::Nil)),
                });
            }
            LListT::Nil => {
                *self = *Box::new(LListT::Element {
                    elem: value,
                    next: Box::new(mem::replace(self, LListT::Nil)),
                });
            }
        }
    }

    pub fn prepend_easy(&mut self, value: T) {
        println!("Prepending");
        *self = *Box::new(LListT::Element {
            elem: value,
            next: Box::new(mem::replace(self, LListT::Nil)),
        });
    }

    pub fn append(&mut self, value: T) {
        println!("Appending");
        let mut current = self;
        while let LListT::Element { elem: _, next } = current {
            current = next;
        }
        *current = LListT::Element { elem: value, next: Box::new(LListT::Nil) };
    }

    pub fn to_string(&self) {
        println!("Printing");
        let mut index = 0;
        let mut current = self;
        while let LListT::Element { elem, next } = current {
            println!("{} : {}", index, elem);
            index += 1;
            current = next;
        }
    }
}

