use crate::working_with_traits02::drawable::Drawable;

pub struct Button {
    x: u64,
    y: u64,
    h: u64,
    w: u64,
    label: &'static str,
}

impl Button {
    pub fn new(x: u64, y: u64, h: u64, w: u64, label: &'static str) -> Button {
        Button {
            x,
            y,
            h,
            w,
            label,
        }
    }
}

impl Drawable for Button {
    fn draw(&self) {
        println!("Button with x: {}, y: {}, h: {}, w {} and label: {}", self.x, self.y, self.h, self.w, self.label)
    }
}

pub struct Checkbox {
    x: u64,
    y: u64,
    h: u64,
    w: u64,
    label: &'static str,
    is_checked: bool,
}

impl Checkbox {
    pub fn new(x: u64, y: u64, h: u64, w: u64, label: &'static str, is_checked: bool) -> Checkbox {
        Checkbox {
            x,
            y,
            h,
            w,
            label,
            is_checked,
        }
    }
}

impl Drawable for Checkbox {
    fn draw(&self) {
        println!("Checkbox with x: {}, y: {}, h: {}, w {} and label: {} and is_checked: {} ", self.x, self.y, self.h, self.w, self.label, self.is_checked)
    }
}

pub struct TextField {
    x: u64,
    y: u64,
    h: u64,
    w: u64,
    text: String,
}

impl TextField {
    pub fn new(x: u64, y: u64, h: u64, w: u64, text: String) -> TextField {
        TextField {
            x,
            y,
            h,
            w,
            text,
        }
    }

    pub fn add(&mut self, c: char) {
        self.text.push(c)
    }
}

impl Drawable for TextField {
    fn draw(&self) {
        println!("TextField with x: {}, y: {}, h: {}, w {} and text: {} ", self.x, self.y, self.h, self.w, self.text)
    }
}