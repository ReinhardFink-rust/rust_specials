use std::fmt::Error;

pub fn working_with_results() {

    let mut _r : Result<i32,Error> = Result::Ok(5);
    match _r.unwrap() {
        5 => println!("{:?} ", _r),
        _ => println!("Nix"),
    }

    match _r {
        Ok(5) => println!("{:?} ", _r),
        _ => println!("Nix"),
    }

    /*
    not working
    let _s = _r.map(|i| i += 1); i cannot be changed
    let _s = _r.map(|i| j = i + 1); j is not known
     */
    let _s = _r.map(|i| i + 1);
    println!("{:?} ", _s);

}