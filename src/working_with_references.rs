
pub fn working_with_references() {

    immutable_and_mutable_variables();

    immutable_reference_to_mutabel_variables();

    println!("references in declarations for immutable & mutable variable");
    let &_immut_a: &i32 = &3;
    // _immut_a is i32
    assert_eq!(_immut_a, 3);
    // &_immut_a is &i32
    assert_eq!(&_immut_a, &3);
    // derefernce => _immut_a is i32
    assert_eq!(*&_immut_a, *&3);

    let mut _mut_a = 3;
    // _mut_a is i32
    assert_eq!(_mut_a, 3);
    _mut_a += 1;
    assert_eq!(_mut_a, 4);
    // mutable reference to mutable variable
    let mut _ref_mut_a = &_mut_a;
    assert_eq!(_ref_mut_a, &4);
    assert_eq!(*_ref_mut_a, 4);

    let &mut mut _mut_b : &mut i32 = &mut 3;
    assert_eq!(_mut_b, 3);
    _mut_b += 1;
    assert_eq!(_mut_b, 4);
    // the next line will not work:  cannot use `+=` on type `&mut i32`
    // &mut _mut_b += 1;
    *&mut _mut_b += 1;
    assert_eq!(_mut_b, 5);

    reference_tests();

}

fn immutable_and_mutable_variables() {
    println!("immutable & mutable variables");
    let _immut_a = 4;
    //_a += 1; won't work
    // copy _a into mut _b
    let mut _mut_a = _immut_a;
    assert_eq!(_immut_a, 4);
    assert_eq!(_mut_a, 4);
    _mut_a += 1;
    assert_eq!(_mut_a, 5);
}

fn immutable_reference_to_mutabel_variables() {
    println!("immutable references to mutable variables");
    let mut _mut_a = 5;
    let _immut_a = &_mut_a;
    // _immut_a` is a `&` reference, so the data it refers to, cannot be written
    // *_immut_a += 1; // won't work
    // reference to reference
    assert_eq!(_immut_a, &5);
    // dereference to i32
    assert_eq!(*_immut_a, 5);
    // and reference again
    assert_eq!(&*_immut_a, &5);
    // reference of a reference
    assert_eq!(&_immut_a, &&5);
}

fn reference_tests() {
    let mut a = 3;
    println!("{a}");
    a = 4;
    println!("{a}");
    let pa1 = &a;
    println!("{pa1}");
    let pa2 = &mut a;
    println!("{pa2}");
    *pa2 = 5;
    println!("{pa2}");
    // geht nicht, weil:  6 am Statmentende wieder freigegeben wird pa2 = &mut 6;
    let pa3 = &mut a;
    println!("{pa3}");
    *pa3 = 6;
    println!("{pa3}");
    // geht nicht, weil: 1. borrow nach 2. benuetzt println!("{pa2}");
    // geht nicht, weil: immuatble borrow nach mutable borrow println!("{pa1}");
    let _b = 2;
    // geht nicht, weil: b immutable pa3 = &mut b;
    let mut _b = 2;
    // geht nicht, weil: pa3 immutable pa3 = &mut b;
    let mut pa4 = &mut a;
    println!("{pa4}");
    pa4 = &mut _b;
    println!("{pa4}");
}