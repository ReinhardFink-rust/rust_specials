use std::fmt::{Display};
use std::ops::{Add, Sub};

struct Matrix<T> {
    a11 : T,
    a12 : T,
    a21 : T,
    a22 : T
}

impl<T> Matrix<T> {
    fn new(a11: T, a12: T, a21: T, a22: T) -> Self {
        Matrix {
            a11,
            a12,
            a21,
            a22
        }
    }
}

impl<T : Clone> Clone for Matrix<T> {
    fn clone(&self) -> Self {
        Matrix {
            a11 : self.a11.clone(),
            a12 : self.a12.clone(),
            a21 : self.a21.clone(),
            a22 : self.a22.clone(),
        }
    }
}

impl<T : Copy> Copy for Matrix<T> { }

impl<T : Display> ToString for Matrix<T> {
    fn to_string(&self) -> String {
        let mut s = String::new();
        s = s.add("[ ");
        s = s.add(&self.a11.to_string());
        s = s.add("\t\t");
        s = s.add(&self.a12.to_string());
        s = s.add(" ]\n");
        s = s.add("[ ");
        s = s.add(&self.a21.to_string());
        s = s.add("\t\t");
        s = s.add(&self.a22.to_string());
        s = s.add(" ]");
        //, &self.a12));
        //s.add(format!("[ {}  {} ]\n", &self.a21, &self.a22));
        s
    }
}

// T has implement type Add as well
// but if Add is implemented, we have to tell which type = Output T returns
// therefore tricky :-) T : Add<Output = T>
impl<T : Add<Output = T>> Add for Matrix<T> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        Self {
            a11 : self.a11 + rhs.a11,
            a12 : self.a12 + rhs.a12,
            a21 : self.a21 + rhs.a21,
            a22 : self.a22 + rhs.a22
        }
    }
}

impl<T> Sub for Matrix<T>
    where T : Sub<Output = T>
{
    type Output = Self;

    fn sub(self, rhs: Self) -> Self {
        Self {
            a11 : self.a11 - rhs.a11,
            a12 : self.a12 - rhs.a12,
            a21 : self.a21 - rhs.a21,
            a22 : self.a22 - rhs.a22
        }
    }
}

pub fn working_with_traits03() {
    let mut _m1 = Matrix::new(1,0,0,1);
    let mut _m2 = Matrix::new(1,0,0,1);
    let mut _m3 = _m1 + _m2;
    println!("{}", _m3.to_string());
    let mut _m1 = Matrix::new(1.1,0.1,0.1,1.1);
    let mut _m2 = Matrix::new(1.0,1.0,1.0,1.0);
    let mut _m3 = _m1 - _m2;
    println!("{}", _m3.to_string());
    _m3 = _m1 - _m2 - _m1;
    println!("{}", _m3.to_string());
}
