pub fn working_with_boxes() {

    println!("working with Boxes");

    println!("get contend from a Box");
    let mut _b = Box::new(1);
    // cannot use `+=` on type `std::boxed::Box<{integer}>
    // _b += 1;
    *_b += 1;
    assert_eq!(*_b, 2);
    // automatic dereference in println!
    println!("b: {}", _b);
    println!("b: {}", *_b);

    println!("Box with copy trait contend");
    let mut _b1 = Box::new(1);
    let mut _b2 = Box::new(2);
    println!("b1: {},  b2: {} ", _b1, _b2);
    assert_eq!(*_b1, 1);
    assert_eq!(*_b2, 2);
    let _temp = _b1;
    _b1 = _b2;
    _b2 = _temp;
    println!("b1: {},  b2: {} ", _b1, _b2);
    assert_eq!(*_b1, 2);
    assert_eq!(*_b2, 1);

    #[derive(Debug)]
    struct A {
        x : String,
        y : i32,
    }
    let mut _b1 = Box::new(A{x: "hello".to_string(), y: 12});
    let mut _b2 = Box::new(A{x: "World".to_string(), y: 24});
    // read A::x and A::y to prevent warning: field is never read: `x`
    let _x = &_b1.x;
    let _y = &_b1.y;
    println!("b1: {:?},  b2: {:?} ", _b1, _b2);
    let _temp = _b1;
    _b1 = _b2;
    _b2 = _temp;
    println!("b1: {:?},  b2: {:?} ", _b1, _b2);

    println!("Box in an array");
    let mut _arr = [_b1, _b2];
    println!("arr[0]: {:?},  arr[1] {:?} ", _arr[0], _arr[1]);
    _arr[1] = Box::new(A{x: "beautyful".to_string(), y: 42});
    println!("arr[0]: {:?},  arr[1] {:?} ", _arr[0], _arr[1]);
    // swap WILL NOT WORK
    //let _temp = _arr[1]; <- cannot move out of here
    //_arr[1] = _arr[2];
    //_arr[2] = _temp;
    //_arr.swap(0,1);
    println!("arr[0]: {:?},  arr[1] {:?} ", _arr[0], _arr[1]);

    println!("Boxes via &");

    let mut _b : Box<i32>= Box::new(1);
    _b = Box::new(2);
    println!("{}", _b);
    _b = new_box(3);
    println!("{}", _b);

    let mut _c = &_b;
    // assert_eq!(_c, 3); // no implementation for `&std::boxed::Box<i32> == {integer}
    // assert_eq!(*_c, 3); // expected struct `std::boxed::Box`, found integer
    assert_eq!(**_c, 3);
}

fn new_box(i : i32) -> Box<i32> {

    Box::new(i)
}