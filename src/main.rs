mod working_with_references;
mod working_with_functions;
mod working_with_enums01;
mod working_with_boxes;
mod working_with_traits01;
mod working_with_traits02;
mod working_with_traits03;
mod working_with_traits04;
mod working_with_linked_lists01;
mod working_with_linked_lists02;
mod working_with_linked_lists03;
mod working_with_results;
mod working_with_type_states;
mod working_with_macros;
mod working_with_iterators;
mod working_with_the_borrow_checker;

mod run_test;

use crate::working_with_references::working_with_references;
use crate::working_with_functions::working_with_functions;
use crate::working_with_enums01::working_with_enums01;
use crate::working_with_results::working_with_results;
use crate::working_with_boxes::working_with_boxes;
use crate::working_with_traits01::working_with_traits01;
use crate::working_with_traits02::working_with_traits02;
use crate::working_with_traits03::working_with_traits03;
use crate::working_with_traits04::working_with_traits04;
use crate::working_with_linked_lists01::working_with_linked_lists01;
use crate::working_with_linked_lists02::working_with_linked_lists02;
use crate::working_with_linked_lists03::working_with_linked_lists03;
use crate::working_with_type_states::*;
use crate::working_with_macros::working_with_macros;
use crate::working_with_iterators::working_with_iterators;
use crate::working_with_the_borrow_checker::working_with_the_borrow_checker;


fn main() {
    println!("Hello, to Rust Specials!");

    run_something_1();
    println!("### WORKING WITH REFERENCES ###");
    working_with_references();
    println!("### WORKING WITH FUNCTIONS ###");
    working_with_functions();
    println!("### WORKING WITH ENUMS ###");
    working_with_enums01();
    println!("### WORKING WITH RESULTS ###");
    working_with_results();
    println!("### WORKING WITH BOXES ###");
    working_with_boxes();
    println!("### WORKING WITH TRAITS 01 ###");
    working_with_traits01();
    println!("### WORKING WITH TRAITS 02 ###");
    working_with_traits02();
    println!("### WORKING WITH TRAITS 03 ###");
    working_with_traits03();
    println!("### WORKING WITH TRAITS 04 ###");
    working_with_traits04();
    println!("### WORKING WITH LINKED LISTS 01 ###");
    working_with_linked_lists01();
    println!("### WORKING WITH LINKED LISTS 02 ###");
    working_with_linked_lists02();
    println!("### WORKING WITH LINKED LISTS 03 ###");
    working_with_linked_lists03();
    println!("### WORKING WITH TYPE STATES ###");
    working_with_type_states();
    println!("### WORKING WITH MACROS ###");
    working_with_macros();
    println!("### WORKING WITH ITERATORS ###");
    working_with_iterators();
    println!("### WORKING WITH THE BORROW CHECKER ###");
    working_with_the_borrow_checker()
}

fn run_something_1() {
    let option : Option<i32> = Some(5);
    println!("Option {:?}: ", option);
    println!("Option.unwrap() {:?}: ", option.unwrap());
    println!("Option.as_ref() {:?}: ", option.as_ref());

    let _x = 10;
    let option : Option<i32> = Some(_x);
    println!("Option {:?}: ", option);
    println!("Option.unwrap() {:?}: ", option.unwrap());
    println!("Option.as_ref() {:?}: ", option.as_ref());
    //println!("Option.as_mut() {:?}: ", option.as_mut());
    //let mut y = option.as_mut().unwrap();

    let mut _x = 20;
    let mut option : Option<i32> = Some(_x);
    println!("Option {:?}: ", option);
    println!("Option.unwrap() {:?}: ", option.unwrap());
    println!("Option.as_ref() {:?}: ", option.as_ref());
    println!("Option.as_mut() {:?}: ", option.as_mut());
    let mut _ay = option.as_mut();
    let mut _y = option.as_mut().unwrap();
    *_y = *_y + 1;
    println!("y: {}", _y);
    *_y = *_y + 1;
    println!("y: {}", _y);
    //let mut z = option.as_mut().unwrap();
    //*z = *z + 1;
    //println!("z: {}", z);
    *_y = *_y + 1;

    let _some_3 = Some(3);
    //let Some(x) = some_3;
    //    ^^^^^^^ pattern `None` not covered

}
