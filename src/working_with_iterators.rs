pub fn working_with_iterators() {
    println!("work with iterators");

    let mut list = Vec::new();
    list.push("one");
    list.push("two");
    list.push("three");
    list.push("four");
    //list.push("one");
    println!("for e in list.iter()");
    for e in list.iter() {
        println!("{}", e);
    }
    println!("for e in list.iter().rev()");
    for e in list.iter().rev() {
        println!("{}", e);
    }
    println!("for e in list.iter().enumerate()");
    for e in list.iter().enumerate() {
        println!("{}:{}", e.0, e.1);
    }
    println!("for e in list.iter().enumerate().rev()");
    for e in list.iter().enumerate().rev() {
        println!("{}:{}", e.0, e.1);
    }
    println!("for e in list.iter().rev().enumerate()");
    for e in list.iter().rev().enumerate() {
        println!("{}:{}", e.0, e.1);
    }
    println!("for e in list.iter().filter(|x| x.to_string().starts_with(\"t\")).enumerate()");
    for e in list.iter().filter(|x| x.to_string().starts_with("t")).enumerate() {
        println!("{}:{}", e.0, e.1);
    }
    println!("for e in list.iter().enumerate().filter(|x| x.1.to_string().starts_with(\"t\"))");
    for e in list.iter().enumerate().filter(|x| x.1.to_string().starts_with("t")) {
        println!("{}:{}", e.0, e.1);
    }
    let list_n = vec![10, 11, 12, 13];
    println!("listN.iter().fold(0, |acc, x| acc + x)");
    let s = list_n.iter().fold(0, |acc, x| acc + x);
    println!("{:?}", s);
    println!("let mut s = list.iter().fold( String::from(\"START: \"), |mut acc, e| \
    {{ acc.push_str(e);
        if e != list.last().unwrap() {{
            acc.push('_');
        }}
        String::from(acc)
    }});");
    let s = list.iter().fold( String::from("START: "), |mut acc, e| {
        acc.push_str(e);
        if e != list.last().unwrap() {
            acc.push('_');
        }
        String::from(acc)
    });
    println!("{}", s);
}

